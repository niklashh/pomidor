use notify_rust::Notification;
use std::io::Read;
use std::io::Write;
use std::thread;
use std::time::{Duration, Instant};

pub struct Config {
    work_duration: Duration,
    break_duration: Duration,
}

impl Config {
    pub fn new(work_duration: Duration, break_duration: Duration) -> Config {
        Config {
            work_duration,
            break_duration,
        }
    }

    pub fn from_minutes(work_minutes: f64, break_minutes: f64) -> Config {
        Config::new(
            Duration::from_secs_f64(60. * work_minutes),
            Duration::from_secs_f64(60. * break_minutes),
        )
    }
}

#[derive(Default)]
pub struct Spent {
    working: Duration,
    slacking: Duration,
}

pub fn wait_input() {
    std::io::stdin().read(&mut [0]).unwrap();
}

pub fn cycle(config: &Config, spent: &mut Spent, start: &Instant) -> Instant {
    std::io::stdout()
        .write("start workink?".as_bytes())
        .unwrap();
    std::io::stdout().flush().unwrap();
    wait_input();
    spent.slacking += start.elapsed();

    let start = Instant::now();
    thread::sleep(config.work_duration);

    notify_break();
    std::io::stdout().write("take brek?".as_bytes()).unwrap();
    std::io::stdout().flush().unwrap();
    wait_input();
    spent.working += start.elapsed();

    let start = Instant::now();
    thread::sleep(config.break_duration);

    notify_work();
    start
}

pub fn notify_work() {
    Notification::new().summary("Get back to work");
}

pub fn notify_break() {
    Notification::new().summary("Take a brek");
}

pub fn run(config: &Config) {
    let mut spent = Spent::default();
    let mut start = Instant::now();

    loop {
        start = cycle(&config, &mut spent, &start);
    }
}
